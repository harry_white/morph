import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-risk',
  templateUrl: './risk.component.html',
  styleUrls: ['./risk.component.scss']
})
export class RiskComponent implements OnInit {

  inc = 100;
  n = 21;
  changing = 0;
  covariance = -0.1;

  brandA = {
    investment: 1500,
    ci: 0.1,
    demand: {
      value: 0.7,
      range: 0.1,
      lowerRange: 0,
      upperRange: 0
    },
    media: {
      value: 0.9,
      range: 0.3,
      lowerRange: 0,
      upperRange: 0
    },
    alpha: {
      sales: 1000,
      morphSales: 0,
      morphSalesUpper: 0,
      morphSalesLower: 0
    },
    beta: {
      sales: 4000,
      morphSales: 0,
      morphSalesUpper: 0,
      morphSalesLower: 0
    },
    data: new Array(this.n),
    table: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    y: 0,
    weight: 0,
  };

  brandB = {
    investment: 1500,
    ci: 0.1,
    demand: {
      value: 0.7,
      range: 0.1,
      lowerRange: 0,
      upperRange: 0
    },
    media: {
      value: 0.9,
      range: 0.3,
      lowerRange: 0,
      upperRange: 0
    },
    alpha: {
      sales: 1000,
      morphSales: 0,
      morphSalesUpper: 0,
      morphSalesLower: 0
    },
    beta: {
      sales: 4000,
      morphSales: 0,
      morphSalesUpper: 0,
      morphSalesLower: 0
    },
    data: new Array(this.n),
    table: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    y: 0,
    weight: 0,
  };

  sum = {
    investment: 1500,
    ci: 0.1,
    demand: {
      value: 0,
      range: 0,
      lowerRange: 0,
      upperRange: 0
    },
    media: {
      value: 0,
      range: 0,
      lowerRange: 0,
      upperRange: 0
    },
    alpha: {
      sales: 0,
      morphSales: 0,
      morphSalesUpper: 0,
      morphSalesLower: 0
    },
    beta: {
      sales: 0,
      morphSales: 0,
      morphSalesUpper: 0,
      morphSalesLower: 0
    },
    data: new Array(this.n),
    table: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    y: 0,
    weight: 0,
  };

  ngOnInit() {
    this.runModel();
  }
  runModel() {
    this.runData(this.brandA);
    this.runData(this.brandB);
    const allY = this.brandA.data.map(data => data.uci).concat(this.brandB.data.map(data => data.uci))
    this.brandA.y = Math.max(...allY)
    this.brandB.y = Math.max(...allY)
    this.changing++;
  }
  runData(brand) {

    brand.demand.lowerRange = brand.demand.value * (1 - (brand.demand.range));
    brand.demand.upperRange = brand.demand.value * (1 + (brand.demand.range));
    brand.media.lowerRange = brand.media.value * (1 - (brand.media.range));
    brand.media.upperRange = brand.media.value * (1 + (brand.media.range));

    brand.alpha.morph = brand.alpha.sales * brand.media.value;
    brand.alpha.morphSalesLower = brand.alpha.sales * brand.media.upperRange;
    brand.alpha.morphSalesUpper = brand.alpha.sales * brand.media.lowerRange;

    brand.beta.morph = brand.beta.sales * brand.demand.value;
    brand.beta.morphSalesLower = brand.beta.sales * brand.demand.lowerRange;
    brand.beta.morphSalesUpper = brand.beta.sales * brand.demand.value;

    for (let i = 0; i < this.n; i++) {
      brand.data[i] = {};
      brand.data[i].media = i * this.inc;
      brand.data[i].sales = brand.beta.sales * (1 - Math.exp(-brand.data[i].media / brand.alpha.sales));
      brand.data[i].lci = brand.data[i].sales * (1 - Number(brand.ci));
      brand.data[i].uci = brand.data[i].sales * (1 + Number(brand.ci));
      brand.data[i].morphSales = brand.beta.morph * (1 - Math.exp(-brand.data[i].media / brand.alpha.sales));
      brand.data[i].morphLowerSales = brand.beta.morphSalesLower * (1 - Math.exp(-brand.data[i].media / brand.alpha.morphSalesLower));
      brand.data[i].morphUpperSales = brand.beta.morphSalesUpper * (1 - Math.exp(-brand.data[i].media / brand.alpha.morphSalesUpper));
    }
  }
  chartChange(event) {
    if (event.chart === 'chartA') {
      this.brandA.investment = event.position;
      this.brandA.table[0] = (1 - Math.exp(-this.brandA.investment / this.brandA.alpha.sales)) * this.brandA.beta.sales;
      this.brandA.table[1] = this.brandA.table[0] - this.brandA.investment;
      this.brandA.table[2] = this.brandA.table[0] / this.brandA.investment;
      this.brandA.table[3] = this.brandA.table[1] / this.brandA.investment;
      this.brandA.table[4] = (this.brandA.table[0] * (1 + Number(this.brandA.ci)) - this.brandA.table[0] * (1 - Number(this.brandA.ci)));
      this.brandA.table[5] = this.brandA.table[4] / this.brandA.investment;
      this.brandA.table[6] = this.brandA.table[4] / this.brandA.table[0];
      this.brandA.table[7] = this.brandA.table[4] / this.brandA.table[1];
      this.brandA.table[8] = this.brandA.table[7] / this.brandA.investment * 100;
      this.brandA.table[9] = this.brandA.table[4] / this.brandA.table[2] * 100;
      this.brandA.table[10] = this.brandA.table[4] / this.brandA.table[3] * 100;
    } else {
      this.brandB.investment = event.position;
      this.brandB.table[0] = (1 - Math.exp(-this.brandB.investment / this.brandB.alpha.sales)) * this.brandB.beta.sales;
      this.brandB.table[1] = this.brandB.table[0] - this.brandB.investment;
      this.brandB.table[2] = this.brandB.table[0] / this.brandB.investment;
      this.brandB.table[3] = this.brandB.table[1] / this.brandB.investment;
      this.brandB.table[4] = (this.brandB.table[0] * (1 + Number(this.brandB.ci)) - this.brandB.table[0] * (1 - Number(this.brandB.ci)));
      this.brandB.table[5] = this.brandB.table[4] / this.brandB.investment;
      this.brandB.table[6] = this.brandB.table[4] / this.brandB.table[0];
      this.brandB.table[7] = this.brandB.table[4] / this.brandB.table[1];
      this.brandB.table[8] = this.brandB.table[7] / this.brandB.investment * 100;
      this.brandB.table[9] = this.brandB.table[4] / this.brandB.table[2] * 100;
      this.brandB.table[10] = this.brandB.table[4] / this.brandB.table[3] * 100;

    }
    this.sum.investment = this.brandA.investment + this.brandB.investment;
    this.sum.table[0] = this.brandA.table[0] + this.brandB.table[0];
    this.sum.table[1] = this.brandA.table[1] + this.brandB.table[1];

    this.sum.table[2] = this.sum.table[0] / this.sum.investment;
    this.sum.table[3] = this.sum.table[1] / this.sum.investment;


    this.sum.table[5] = this.sum.table[4] / this.sum.investment;
    this.sum.table[6] = this.sum.table[4] / this.sum.table[0];
    this.sum.table[7] = this.sum.table[4] / this.sum.table[1];
    this.sum.table[8] = this.sum.table[7] / this.sum.investment * 100;
    this.sum.table[9] = this.sum.table[4] / this.sum.table[2] * 100;
    this.sum.table[10] = this.sum.table[4] / this.sum.table[3] * 100;

    this.brandA.weight = this.brandA.investment / this.sum.investment;
    this.brandB.weight = this.brandB.investment / this.sum.investment;

    this.sum.table[4] = Math.pow(Math.pow(this.brandA.weight, 2) * Math.pow(this.brandA.table[4], 2) +
      Math.pow(this.brandB.weight, 2) * Math.pow(this.brandB.table[4], 2) +
      2 * this.brandA.weight * this.brandB.weight * this.covariance * this.brandA.table[4] * this.brandB.table[4], 0.5)

  }
}

