import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vertical-slider',
  templateUrl: './vertical-slider.component.html',
  styleUrls: ['./vertical-slider.component.scss']
})
export class VerticalSliderComponent implements OnInit {

  sliding = false;
  handlePosition = 16;

  constructor() { }

  ngOnInit(): void {
  }
  startSlide() {
    this.sliding = true;
  }
  moveSlider(e) {
    if (this.sliding) {
      this.handlePosition = (e.offsetY - 12);
    }
  }
  endSlide() {
    this.sliding = false;
  }
}
