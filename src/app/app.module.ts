import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RiskComponent } from './risk/risk.component';
import { LineDragComponent } from './d3/line-drag/line-drag.component';

import { FormsModule } from '@angular/forms';
import { VerticalSliderComponent } from './vertical-slider/vertical-slider.component';

@NgModule({
  declarations: [
    AppComponent,
    RiskComponent,
    LineDragComponent,
    VerticalSliderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
