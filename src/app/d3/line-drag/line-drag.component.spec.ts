import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineDragComponent } from './line-drag.component';

describe('LineDragComponent', () => {
  let component: LineDragComponent;
  let fixture: ComponentFixture<LineDragComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineDragComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineDragComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
