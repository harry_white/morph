import { Component, OnInit, Input, HostListener, EventEmitter, Output } from '@angular/core';
import * as d3 from 'd3';
import { config } from 'rxjs';

@Component({
  selector: 'app-line-drag',
  templateUrl: './line-drag.component.html',
  styleUrls: ['./line-drag.component.scss']
})
export class LineDragComponent implements OnInit {
  @Input() config;
  @Input() chartId;
  @Input() changing;
  @Input() y;
  data
  @Output() voted = new EventEmitter<any>();
  title;
  dragging = false;
  inited = false;
  ngOnInit() { }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.drawChart();
  }

  ngAfterViewInit() {
    this.drawChart();
    this.inited = true;
  }

  ngOnChanges() {
    if (this.inited) {
      this.drawChart();
    }
  }

  drawChart() {
    this.title = 'Brand ' + this.chartId.substring(this.chartId.length - 1)
    const that = this;

    const margin = { top: 32, right: 20, bottom: 40, left: 74 };
    const width = ((window.innerWidth - 64) / 2) - margin.left - margin.right - 32;
    const height = 240 - margin.top - margin.bottom;

    const x = d3.scaleLinear().range([0, width]);
    const y = d3.scaleLinear().range([height, 0]);

    const sales = d3.line()
      .x(d => x(d['media']) as any)
      .y(d => y(d['sales']) as any);

    // const lci = d3.line()
    //   .x(d => x(d['media']) as any)
    //   .y(d => y(d['uci']) as any);

    //   const uci = d3.line()
    //   .x(d => x(d['media']) as any)
    //   .y(d => y(d['lci']) as any);

    const morphSales = d3.line()
      .x(d => x(d['media']) as any)
      .y(d => y(d['morphSales']) as any);

    const lowerMorphSales = d3.line()
      .x(d => x(d['media']) as any)
      .y(d => y(d['morphLowerSales']) as any);

    const upperMorphSales = d3.line()
      .x(d => x(d['media']) as any)
      .y(d => y(d['morphUpperSales']) as any);

    //#region AREA
    // define the area
    const areaHigh = d3.area()
      .x(d => x(d['media']) as any)
      .y0(d => y(d['morphSales']) as any)
      .y1(d => y(d['morphUpperSales']) as any);
    const areaLow = d3.area()
      .x(d => x(d['media']) as any)
      .y0(d => y(d['morphSales']) as any)
      .y1(d => y(d['morphLowerSales']) as any);
    //#endregion

    d3.select('#' + this.chartId).select('svg').remove();

    const svg = d3.select('#' + this.chartId).append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .on("mousemove", mouse)
      .on('mouseup', function () {
        that.dragging = false;
      })
      .append('g')
      .attr('transform',
        'translate(' + margin.left + ',' + margin.top + ')');

    x.domain([0, d3.max(this.config.data, d => d['media'])] as any);
    y.domain([0, this.y] as any);

    const path = svg.append('path')
      .data([this.config.data])
      .attr('class', 'line sales-line')
      .attr('d', sales as any);

    // svg.append('path')
    //   .data([this.config.data])
    //   .attr('class', 'line')
    //   .attr('d', lci as any);

    // svg.append('path')
    //   .data([this.config.data])
    //   .attr('class', 'line')
    //   .attr('d', uci as any);



    // svg.append('path')
    //   .data([this.config.data])
    //   .attr('class', 'line')
    //   .attr('d', lowerMorphSales as any);

    // svg.append('path')
    //   .data([this.config.data])
    //   .attr('class', 'line')
    //   .attr('d', upperMorphSales as any);

    // Add the area.
    svg.append("path")
      .data([this.config.data])
      .attr("class", "area high")
      .attr("d", areaHigh);

    svg.append("path")
      .data([this.config.data])
      .attr("class", "area low")
      .attr("d", areaLow);

    svg.append('path')
      .data([this.config.data])
      .attr('class', 'line morph-line')
      .attr('d', morphSales as any);

    svg.append("circle")
      .data(this.config.data.map(data => [data.media, data.sales]))
      .attr("r", 4)
      .attr("transform", function (data) {
        return "translate(" + data + ")";
      })
      .on('mousedown', function () {
        that.dragging = true;
      })
      .on('mouseup', function () {
        that.dragging = false;
      });

    svg.append('g')
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(x));

    svg.append('g')
      .call(d3.axisLeft(y));


    const circles = [{
      x: 0,
      y: 0,
    }];

    svg.selectAll("circle")
      .data(circles)
      .join("circle")
      .attr('id', 'drag-handle')
      .attr("cx", d => d.x)
      .attr("cy", d => d.y)
      .attr("r", 8)
      .attr("fill", '#2e86de')


    positionHandle(0.5);
    // .enter()
    // .tween("pathTween", pathTween); //Custom tween to set the cx and cy attributes


    // svg.append("circle")
    //   .attr("cx", 25) //Starting x
    //   .attr("cy", 25) //Starting y
    //   .attr("r", 25)
    //   .transition()
    //   .delay(250)
    //   .duration(1000)
    //   .on("mousemove", mouse)

    //   // .ease("linear")
    //   .tween("pathTween", function () { return pathTween(path) })
    // // .call(drag);
    // // .tween("pathTween", pathTween); //Custom tween to set the cx and cy attributes


    // function pathTween(path) {
    //   var length = path.node().getTotalLength(); // Get the length of the path
    //   var r = d3.interpolate(0, length); //Set up interpolation from 0 to the path length
    //   return function (t) {
    //     var point = path.node().getPointAtLength(r(t)); // Get the next point along the path
    //     d3.select(this) // Select the circle
    //       .attr("cx", point.x) // Set the cx
    //       .attr("cy", point.y) // Set the cy
    //   }
    // }
    function mouse(d) {
      let xPosition = d3.mouse(this)[0] - margin.left;
      if (xPosition > width) { xPosition = width; }
      if (xPosition < 0) { xPosition = 0; }
      const xValue = xPosition / width * x.domain()[1]
      const yValue = that.config.beta.sales * (1 - Math.exp(-xValue / that.config.alpha.sales))
      const yPosition = (height - (yValue / that.config.y * height));

      if (that.dragging) {
        const handlePosititon = {
          position: xValue,
          chart: that.chartId
        }
        that.voted.emit(handlePosititon);
        const t = (d3.mouse(this)[0] - margin.left) / width;
        // d.x = d3.mouse(this)[0]
        // d3.select(this)
        //   .attr('cx', d3.mouse(this)[0])
        //   .attr('cy', d3.mouse(this)[1])

        svg
          .select('#drag-handle') // Select the circle
          .attr("cx", xPosition) // Set the cx
          .attr("cy", yPosition); // Set the cy
        // const t = 0.9
        // positionHandle(t);
      }
    }

    function positionHandle(t) {

      var length = path.node().getTotalLength(); // Get the length of the path
      var r = d3.interpolate(0, length); //Set up interpolation from 0 to the path length
      var point = path.node().getPointAtLength(r(t)); // Get the next point along the path
      var handlePosititon = {
        position: x.domain()[1] * t,
        chart: that.chartId
      };
      that.voted.emit(handlePosititon);
      svg
        .select('#drag-handle') // Select the circle
        .attr("cx", point.x) // Set the cx
        .attr("cy", point.y); // Set the cy
    }


  }
}

